package com.shuvi.cinema.service.impl;

import com.shuvi.cinema.controller.dto.notification.NotificationRequest;
import com.shuvi.cinema.service.api.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
@Primary
public class KafkaNotificationService implements NotificationService {

    @Value("{kafka.topic.notification}")
    private String notificationTopic;
    private final KafkaTemplate<String, NotificationRequest> kafkaSender;

    @Override
    public void sendNotification(NotificationRequest notificationRequest) {
        try {
            kafkaSender.send(notificationTopic, notificationRequest);
            log.info("Send Kafka notification success: " + notificationRequest);
        } catch (Exception exc) {
            log.error("Send kafka notification failed, error: {}", exc.getMessage());
        }
    }
}
